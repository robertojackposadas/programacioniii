package programacion3;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import programacion3.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-11T19:50:20")
@StaticMetamodel(Nivel.class)
public class Nivel_ { 

    public static volatile SingularAttribute<Nivel, Integer> codigo;
    public static volatile SingularAttribute<Nivel, Integer> numero;
    public static volatile SingularAttribute<Nivel, Usuario> usuario;
    public static volatile SingularAttribute<Nivel, String> nivel;

}