/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programacion3;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name="niveles")
public class Nivel implements Serializable{
    
    @Id
    @Column(name="codigo")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codigo;
    
    @Column(name="nivel")
    private String nivel;
    
    @Column(name="numero")
    private int numero;

    //@OneToOne(mappedBy = "nivel")
    @OneToOne(mappedBy = "nivel")
    private Usuario usuario;
    
    /*@ManyToOne
    @JoinColumn(name = "nivel")
    private Usuario usuario;
    */
    
    public Nivel() {
    }
/*
    public Nivel(int codigo, String nivel, int numero) {
        this.codigo = codigo;
        this.nivel = nivel;
        this.numero = numero;
    }
*/
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    
}
