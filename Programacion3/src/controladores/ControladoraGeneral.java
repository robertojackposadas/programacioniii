/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import persistencia.ControladoraPersistencia;
import persistencia.NivelJpaController;
import persistencia.UsuarioJpaController;

/**
 *
 * @author Jack
 */
public class ControladoraGeneral extends ControladoraPersistencia{
    private UsuarioJpaController usuarioJPA;
    private NivelJpaController nivelJPA;

    public ControladoraGeneral() {
        this.usuarioJPA = new UsuarioJpaController(getEmf());
        this.nivelJPA = new NivelJpaController(getEmf());
    }

    public UsuarioJpaController getUsuarioJPA() {
        return usuarioJPA;
    }

    public void setUsuarioJPA(UsuarioJpaController usuarioJPA) {
        this.usuarioJPA = usuarioJPA;
    }

    public NivelJpaController getNivelJPA() {
        return nivelJPA;
    }

    public void setNivelJPA(NivelJpaController nivelJPA) {
        this.nivelJPA = nivelJPA;
    }
    
    
    
}
